# sample flipper

# [Update](#update-1)

## One-Sentence-Pitch

Device that converts any inverted microscope into a vertical microscope. 

## How does it work?

The Sample Flipper serves as a mobile version of my vertical confocal microscope, allowing researchers to study (for example) developing plant seedlings, without causing additional stress by laying them on their sides. Paired with my TipTracker software, growing or moving samples can be tracked and imaged over extended periods. 

The Sample Flipper looks like this:

<img src="/img/SampleFlipper01.jpg" width=80% />

It consists of a custom-made stage insert with a servo that actuates a dish holder. Additionally, a calibrated grow light is situated above the sample chamber to maintain healthy plant growth. Between acquisitions, the sample chamber turns the sample vertically/upright, which is particularly beneficial for plant samples, as this orientation aligns with their natural growth pattern – gravity pointing away from the leaves toward the roots and light shining on top of the leaves.

Only during microscope image acquisition does the entire chamber flip/rotate into a horizontal position, with the grow light automatically turning off to avoid impacting image quality. 

A positive stop at the bottom ensures consistent z-positioning:

<img src="/img/bottomstop.jpg" width=80% />

In this position, the sample can be imaged using various advanced imaging modalities, depending on the microscope to which the sample flipper is mounted. 

Here is what an imaging cycle looks like (with custom 30° rotation instead of 90°):

 ![sampleflipper](/img/sampleFlipper.mp4)

 Additionally, a pump is available that carefully dispenses a single drop of immersion liquid (water) before imaging, allowing for higher resolution RI-matched image acquisition. The immersion liquid refill mechanism comprises a flexible tube precisely positioned above the front lens of the microscope objective. The tube is automatically moved out of the way by the chamber when it is rotated to a horizontal position for imaging. Both the chamber's flipping and the water dispersion are managed by a separate program, which is synchronized with the image acquisition process.

 <img src="/img/2022_SampleFlipper_RoHa.jpg" width=80% />

## Update: 

I have added a version of the 96-well plate insert that can be fully 3D printed. 

<img src="/96wellplate/FlipperRender.jpg" width=80% />
<img src="/96wellplate/Photo96Wellplate.jpg" width=80% />


This design is tailored for 1-well chambers, as seen here: [ThermoFisher 1-Well Chamber]( https://www.thermofisher.com/order/catalog/product/155360PK)


Additional Components Required:

[MG90 Micro Servo, 270°](https://shop.mchobby.be/en/servo-motor/1880-mg90-micro-servo-270-metal-analog-feedback-3232100018808-dfrobot.html).

[Micro Maestro 6-Channel USB Servo Controller](https://www.pololu.com/product/1350).
Please note that the servo requires an external 5V power supply and is not USB-powered.

Other Materials Needed:

M2 machine screws (alternatively, glue can be used).

O-rings with ~2.5mm thickness

[A pin measuring 4mm x 32mm]( https://de.rs-online.com/web/p/zylinderstifte/0270603) (longer is preferable).


Assembly Instructions:

The [rotation axis](./96wellplate/rotationAxis.jpg) of the 96-well plate insert is determined by the M4 hole on its side.
Carefully widen this hole and the corresponding hole on the chamber holder. I recommend using a [reamer](https://www.hoffmann-group.com/GB/en/houk/Machining/Drilling/Reaming-tools/Hand-reamers/Hand-reamer-H7/p/160150-4?comingFromCategory=10-01-04-01-00), not a drill, for precision to achieve the required diameter.
The pin should fit securely in the frame's hole and rotate smoothly in the chamber holder. Apply some oil for lubrication on the chamber holder side.
The motor and motor mount can be attached either by screws or glue. Gluing is simpler and prevents lateral force on the axle, although it may not look as neat.

Cut 15 mm pieces from a ~2.5 mm thick o-ring, tapering one end. Insert them from the bottom into the [holes](./96wellplate/oringsgohere.jpg). Use pliers to pull them through, trimming the excess at both the bottom and the top. Taper the top slightly with clippers for easier insertion of the chamber. Insert as many o-ring pieces as necessary to ensure a firm fit for the glass bottom chamber. Finally, permanently affix the top of each piece with glue. It should look like [this]( ./96wellplate/whatoringshouldlooklike.jpg).

You can attach the Pololu housing to the microscope stand using stick-on tape Velcro.

Adjust the lower (imaging) position so that the servo gently presses the chamber holder against the stop plate. This eliminates any play in the servo, ensuring consistent z-positioning of the sample.
For non-time-critical acquisitions (regular, long intervals), start the acquisition manually in the microscope software and simultaneously initiate the flipping-script in the Maestro Control Center.

To-Do:

I plan to add a script for synchronizing acquisition with ZEN controlled microscopes during the flipping process.
Additionally, components for the grow light will be included in future updates.


## Acknowledgment

This has been made possible in part by CZI grant DAF2020-225401 and [grant DOI https://doi.org/10.37921/120055ratwvi](https://doi.org/10.37921/120055ratwvi) from the Chan Zuckerberg Initiative DAF, an advised fund of Silicon Valley Community Foundation (funder DOI 10.13039/100014989).

